#![allow(dead_code)]

use std::{collections::HashMap, io::stdin, sync::Mutex};
use lazy_static::lazy_static;
use lalrpop_util::lalrpop_mod;

mod ast;

type Tag = u64;
type Number = isize;
type Symbol = String;
type Bool = bool;

#[derive(Clone)]
struct Cons {
    car: Exp,
    cdr: Box<Cons>
}

impl std::fmt::Display for Cons {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
	write!(f, "({} . {})", self.car, self.cdr)
    }
}

type List = Cons;

#[derive(Clone)]
enum Sexp {
    Function(Symbol, Args),
    Cons(Cons)
}

impl std::fmt::Display for Sexp {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
	match self {
	    Sexp::Function(func, _) => write!(f, "#'{}", func),
	    Sexp::Cons(cell) => write!(f, "{}", cell)
	}
    }
}

type Args = Vec<Exp>;

#[derive(Clone)]
enum Exp {
    Sexp(Box<Sexp>),
    Literal(Literal)
}

impl std::fmt::Display for Exp {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
	match self {
	    Exp::Sexp(sexp) => write!(f, "{}", *sexp),
	    Exp::Literal(lit) => write!(f, "{}", lit)
	}
    }
}

#[derive(Clone)]
enum Literal {
    Number(Number),
    String(String),
    Symbol(Symbol),
    Bool(Bool),
    Null
}

impl std::fmt::Display for Literal {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
	match self {
	    Literal::Number(a) => write!(f, "{}", a),
	    Literal::String(a) => write!(f, "{}", a),
	    Literal::Symbol(a) => write!(f, "{}", a),
	    Literal::Bool(a) => write!(f, "{}", a),
	    Literal::Null => write!(f, "void")
	}
    }
}

type Atom = Literal;

type Abstraction = (Symbol, Exp);
type Application = (Symbol, Exp);

enum Term {
    Application(Application),
    Abstraction(Abstraction),
    Variable(Symbol)
}

type LexEnv = HashMap<Symbol, Box<Exp>>;

fn exec(s: String) {
    let iter = s.split_whitespace();
    let v: Vec<&str> = iter.collect();
    subprocess::Exec::cmd(v[0]).args(&v[1..]).join().unwrap();
}

fn intern(s: String) -> Symbol {
    s
}

fn cons(a: Exp, b: Exp) -> Cons {
    match b {
	Exp::Sexp(sexp) => todo!{ "Implement cells with sexp as cdr" },
	Exp::Literal(u) => match a {
	    Exp::Sexp(sexp) => {
		let cdr_val = cons(Exp::Literal(u), Exp::Literal(Literal::Null));
		Cons {
		    car: Exp::Sexp(sexp),
		    cdr: Box::new(cdr_val)
		}
	    },
	    
	    Exp::Literal(a) => Cons {
		car: Exp::Literal(a),
		cdr: Box::new(cons(Exp::Literal(u), Exp::Literal(Literal::Null)))
	    }
	}
    }
}

fn car(c: Cons) -> Exp {
    c.car
}

fn cdr(c: Cons) -> Cons {
    *c.cdr
}

lazy_static!{
    static ref SYMTAB: Mutex<LexEnv> = {
	let mut symtab = LexEnv::new();
	symtab.insert(String::from("t"), Box::new(Exp::Literal(Literal::Bool(true))));
	symtab.insert(String::from("nil"), Box::new(Exp::Literal(Literal::Bool(false))));
	Mutex::new(symtab)
    };
}

fn add_to_lexenv(s: Symbol, v: Exp) {
    SYMTAB.lock().unwrap().insert(s, Box::new(v));
}

fn lookup(s: Symbol) -> Exp {
    let mut_lock = SYMTAB.lock().expect("Unable to lock mutex");
    let exp_reference = mut_lock.get(&s).expect("Symbol lookup in lexenv failed");
    let exp = *exp_reference.clone();
    exp
}

fn eval_in_lexenv(term: Term) -> Exp {
    match term {
	Term::Application(app) => {
	    todo!{ "Implement function application" }
	},

	Term::Abstraction((sym, exp)) => {
	    let sym_c = sym.clone();
	    add_to_lexenv(sym_c, exp);
	    Exp::Literal(Literal::Symbol(sym))
	},

	Term::Variable(sym) => {
	    lookup(sym)
	}
    }
}

fn print(exp: Exp) {
    println!("{}", exp);
}

fn repl() {
    loop {
	let mut input = String::new();
	stdin().read_line(&mut input).unwrap();
	let ret = eval_in_lexenv(Term::Variable(String::from("t")));
	print(ret);
	// exec(input);
    }
}

lalrpop_mod!(pub lish);

fn type_of<T>(_: &T) -> String {
    format!("{}", std::any::type_name::<T>())
}

#[test]
fn test_plus_sign() {
    assert!(lish::SignParser::new().parse("+").is_ok());
}

#[test]
fn test_minus_sign() {
    assert!(lish::SignParser::new().parse("-").is_ok());
}

#[test]
fn test_empty_sign() {
    assert!(lish::SignParser::new().parse("").is_ok());
}

#[test]
fn test_invalid_sign() {
    assert!(lish::SignParser::new().parse("sfodisdf").is_err());
}

fn main() {
    let sign = lish::SignParser::new()
        .parse("+").unwrap();
    println!("Parsed + as {:?}; type = {}", sign, type_of(&sign));
    // repl();
}
