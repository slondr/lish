use std::fmt::{Debug, Error, Formatter};

#[derive(Copy, Clone)]
pub enum Sign {
    Positive,
    Negative,
    Empty
}

impl Debug for Sign {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), Error> {
	use self::Sign::*;
	write!(fmt, "{}", match *self {
	    Positive => "+",
	    Negative => "-",
	    Empty => ""
	})
    }
}
